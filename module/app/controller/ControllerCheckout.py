# from crypt import methods
from email.mime import image
from fileinput import filename
import mimetypes
import base64
import os
from urllib import response
import requests
from email import header
from werkzeug.utils import secure_filename

from ..library_ma import CheckoutSchema
from flask import Blueprint, request,jsonify,json, send_from_directory,Flask

app = Flask(__name__)

checkout_schema = CheckoutSchema()
checkouts_schema = CheckoutSchema(many=True)
from ..extensions import db
from ..models.models_product import Checkout
checkout = Blueprint('checkout', __name__)
mylist = []
@checkout.route('/checkout/', methods=['POST'])
def api_checkout():
        address = request.json['address']
        user_id = request.json['user_id']
        user_first_name = request.json['user_first_name']
        user_last_name = request.json['user_last_name']
        price  = request.json['price'] 
        phone =  request.json['phone']
        order_date = request.json['order_date']
        product = request.json['product']
        new_order = Checkout(user_id=user_id,user_first_name=user_first_name,user_last_name=user_last_name,price=price,address=address,phone=phone,order_date=order_date,product=product)
        db.session.add(new_order)
        db.session.commit()
        return checkout_schema.jsonify(new_order)

@checkout.route('/checkout/', methods=['GET'])
def getdatacheckout():
    checkoutss = Checkout.query.all()
    result_get = checkouts_schema.dump(checkoutss)
    return jsonify(result_get)




 