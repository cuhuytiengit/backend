# from crypt import methods
from email.mime import image
from fileinput import filename
import mimetypes
import base64
import os
from urllib import response
import requests
from email import header
# from itertools import user
from werkzeug.utils import secure_filename
from PIL import Image

from ..library_ma import  AdminSchema
from flask import Blueprint, request,jsonify,json, send_from_directory,Flask
from ..extensions import db,ma
from ..models.models_product import Admin
from werkzeug.utils import secure_filename
app = Flask(__name__)
UPLOAD_FOLDER = os.path.join('image-admin')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
 
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
 
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


admin_schema =  AdminSchema()
admins_schema =  AdminSchema(many=True)

admin = Blueprint('admin', __name__)

@admin.route('/get-admin-by-id/<int:id>/', methods=['GET'])
def get_id_admin(id):
    
    if request.method == 'GET':

        admin = Admin.query.get(id)
        if admin:
            return admin_schema.jsonify(admin)
        else:
            return 'not found admin'


# APP_ROOT = os.path.dirname(os.path.abspath(__file__))
# target = os.path.join('images-user')
# print(target)
# if not os.path.isdir(target):
#     os.mkdir(target)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
target = os.path.join(app.root_path,UPLOAD_FOLDER)
if not os.path.isdir(target):
    os.mkdir(target)

@admin.route('/post-admin/', methods=['GET', 'POST'])

def upload_file():
    if request.method == 'POST':
        image_admin = request.files['image_admin']
        print(type("image_admin"))
        abs_file_path = '/'.join([target , image_admin.filename])
        abs_file_path2 = '/'.join([image_admin.filename])
        image_admin.save(abs_file_path)
        print('in ra ' + abs_file_path)
        username =   request.form['username']
        password  =  request.form['password']
        new_admin = Admin(image_admin=abs_file_path2,username=username,password=password)

        print(type(new_admin))
        db.session.add(new_admin)
        db.session.commit()
        return 'success' 
@admin.route('/get-admin/', methods=['GET', 'POST'])
def get_admin():
    if request.method == 'GET':
        admin = Admin.query.all()
        if admin:
            return  admins_schema.jsonify(admin)
        else:
            return 'not found admins'

@admin.route('/image_view_admin/<int:id>/', methods=['GET','POST','OPTIONS'])

def get_file_id(id):

    if request.method == 'GET':
            admin = Admin.query.filter_by(id = id).first()
            if admin:
                full_path = os.path.join(app.root_path,UPLOAD_FOLDER)
                filename = str(admin.image_admin)
                return send_from_directory(  full_path ,filename)
            else:
                return 'not image'
        