# from crypt import methods
from email.mime import image
from fileinput import filename
import mimetypes
import base64
import os
from urllib import response
import requests
from email import header
from werkzeug.utils import secure_filename
import datetime
from ..library_ma import StatisticalSchema,StatisticaluservisitSchema
from flask import Blueprint, request,jsonify,json, send_from_directory,Flask

app = Flask(__name__)

statistical_schema = StatisticalSchema()
statisticals_schema = StatisticalSchema(many=True)
statisticaluservisit_schema = StatisticaluservisitSchema()
statisticaluservisits_schema = StatisticaluservisitSchema(many=True)
from ..extensions import db
from ..models.models_product import Statistical_Visit
from ..models.models_product import Statistical_User_Visit
statistical = Blueprint('statistical', __name__)
mylist = []
@statistical.route('/post_statistical_visit/', methods=['POST'])
def post_statistical_visit():
    ip = request.json['ip']
    new_access = Statistical_Visit(ip=ip)
    db.session.add(new_access)
    db.session.commit()
    return statistical_schema.jsonify(new_access)

@statistical.route('/get_statistical_visit/', methods=['GET'])
def get_statistical_visit():
    get_statistical_visit = Statistical_Visit.query.all()
    result_get = statisticals_schema.dump(get_statistical_visit)
    return jsonify(result_get)

@statistical.route('/statistical_user_visit/', methods=['POST'])
def statistical_user_visit():
    user_id = request.json['user_id']
    date_access = request.json['date_access']
    week_access = request.json['week_access']
    month_access = request.json['month_access']
    new_access = Statistical_User_Visit(user_id=user_id, date_access=date_access, week_access=week_access,month_access=month_access)
    db.session.add(new_access)
    db.session.commit()
    return statistical_schema.jsonify(new_access)

@statistical.route('/get_statistical_user_visit/', methods=['GET'])
def get_statistical_user_visit():
    get_statistical_user_visit = Statistical_User_Visit.query.all()
    result_get = statisticaluservisits_schema.dump(get_statistical_user_visit)
    return jsonify(result_get)
@statistical.route('/get_statistical_user_visit_id/<int:id>/', methods=['GET'])
def get_statistical_user_visit_id(id):
    if request.method == 'GET':
        user_visit = Statistical_User_Visit.query.filter_by(id=id).first()
        if user_visit:
            return statisticaluservisit_schema.jsonify(user_visit)
        else:
            return 'not found user access in id'

@statistical.route('/get_statistical_user_visit/<int:week_access>', methods=['GET'])
def get_statistical_user_visit_week_access(week_access):
    if request.method == 'GET':
        user_visit = Statistical_User_Visit.query.get(week_access)
        if user_visit:
            return statisticaluservisit_schema.jsonify(user_visit)
        else:
            return 'not found user in week'

@statistical.route('/get_statistical_user_visit_week_access/<int:week_access>/', methods=['GET'])
def get_statistical_user_visit_week(week_access):
    if request.method == 'GET':
        user_visit = Statistical_User_Visit.query.filter_by(week_access=week_access).first()
        if user_visit:
            return statisticaluservisit_schema.jsonify(user_visit)
        else:
            return 'not found user access in week_access'
@statistical.route('/get_statistical_user_visit_day/<path:date_access>/', methods=['GET'])
def get_statistical_user_visit_day(date_access):
    if request.method == 'GET':
        user_visit = Statistical_User_Visit.query.filter_by(date_access=date_access).first()
        if user_visit:
            return statisticaluservisit_schema.jsonify(user_visit)
        else:
            return 'not found user access in day'
@statistical.route('/get_statistical_user_visit_month_access/<path:month_access>/', methods=['GET'])
def get_statistical_user_visit_month_access_access(month_access):
    if request.method == 'GET':
        user_visit = Statistical_User_Visit.query.filter_by(month_access=month_access).first()
        if user_visit:
            return statisticaluservisit_schema.jsonify(user_visit)
        else:
            return 'not found user in month_access'

