# from crypt import methods
from email.mime import image
from fileinput import filename
import mimetypes
import base64
import os
from urllib import response
import requests
from email import header
from sqlalchemy import desc,asc

# from itertools import user
from werkzeug.utils import secure_filename
from PIL import Image

from ..library_ma import  UserSchema
from flask import Blueprint, request,jsonify,json, send_from_directory,Flask
from ..extensions import db,ma
from ..models.models_product import User
from werkzeug.utils import secure_filename
app = Flask(__name__)
UPLOAD_FOLDER = os.path.join('image-user')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
 
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
 
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


user_schema =  UserSchema()
users_schema =  UserSchema(many=True)

user = Blueprint('user', __name__)

@user.route('/get-user-by-id/<int:id>/', methods=['GET'])
def get_id_user(id):
    
    if request.method == 'GET':

        user = User.query.get(id)
        if user:
            return user_schema.jsonify(user)
        else:
            return 'not found user'


# APP_ROOT = os.path.dirname(os.path.abspath(__file__))
# target = os.path.join('images-user')
# print(target)
# if not os.path.isdir(target):
#     os.mkdir(target)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
target = os.path.join(app.root_path,UPLOAD_FOLDER)
if not os.path.isdir(target):
    os.mkdir(target)

@user.route('/post-user/', methods=['GET', 'POST'])

def upload_file():
    if request.method == 'POST':
        image = request.files['image']
        print(type("image"))
        abs_file_path = '/'.join([target , image.filename])
        abs_file_path2 = '/'.join([image.filename])
        image.save(abs_file_path)
        print('in ra ' + abs_file_path)
        firstname =   request.form['firstname']
        lastname  =  request.form['lastname']
        email  =  request.form['email']	
        address  =  request.form['address']	
        password  =  request.form['password']	
        phone  =  request.form['phone']	
        
        new_user = User( abs_file_path2,firstname,lastname,email,address,password,phone)

        print(type(new_user))
        db.session.add(new_user)
        db.session.commit()
            


        return 'success' 
    

    if request.method == 'GET':
        user = User.query.all()
        if user:
            return  users_schema.jsonify(user)
        else:
            return 'not found user'

@user.route('/sort-user-a-z/', methods=['GET'])
def sort_az():
    if request.method == 'GET':
        user = User.query.order_by(asc('lastname')).all()
        if user:
            return  users_schema.jsonify(user)
        else:
            return 'not found user'
@user.route('/sort-user-z-a/', methods=['GET'])
def sort_za():
    if request.method == 'GET':
        user = User.query.order_by(desc('lastname')).all()
        if user:
            return  users_schema.jsonify(user)
        else:
            return 'not found user'
@user.route('/delete-user/<int:id>', methods=['DELETE', 'GET'])
def delete_user(id):
    user = User.query.get(id)
    if user:
        try:
            db.session.delete(user)
            db.session.commit()
            return 'user deleted'
        except:
            db.session.rollback()
            return 'cant delete user'
    else:
        return 'not user'
@user.route('/update-user/<int:id>', methods=[ 'GET','POST'])
def update_user(id):    
    if request.method == 'POST':
        user = User.query.get(id)
        image = request.files['image']
        img = Image.open(image)
        abs_file_path = '/'.join([target , image.filename])
        abs_file_path2 = '/'.join([image.filename])

        img.save(abs_file_path)
    
        user.image = secure_filename(abs_file_path2)
        # product.image =  request.form.get('abs_file_path')
        user.firstname =  request.form.get('firstname')
        user.lastname =  request.form.get('lastname')
        user.email =  request.form.get('email')
        user.address =  request.form.get('address')
        user.password =  request.form.get('password')
        user.phone =  request.form.get('phone')

        db.session.commit() 
        return  'User updated'

    else:
        return 'not product'

@user.route('/image_view_user/<int:id>', methods=['GET','POST'])

def get_file_user_id(id):

    user = User.query.filter_by(id = id).first()
    if user:
        full_path = os.path.join(app.root_path,UPLOAD_FOLDER)
        filename = str(user.image)
        return send_from_directory(  full_path ,filename)
    else:
        return 'not image'
@user.route('/get_by_email/<int:email>/', methods=['GET'])

def get_by_email(id):

    user = User.query.get(id)
    if user:
        return user_schema.jsonify(user)
    else:
        return 'not image'