# from crypt import methods
from email.mime import image
from fileinput import filename
import mimetypes
import base64
import datetime
from sqlalchemy import desc,asc
import os
import base64
from functools import wraps
from urllib import response
import requests
from email import header

from werkzeug.utils import secure_filename
from itertools import product
from PIL import Image
import urllib.request as request
import urllib
from werkzeug.datastructures import FileStorage
from ..library_ma import ProductSchema
from flask import Blueprint, request,jsonify,json, send_from_directory,Flask,session,make_response
import jwt
from ..extensions import db,ma
from ..models.models_product import Product
from werkzeug.utils import secure_filename

app = Flask(__name__)
UPLOAD_FOLDER = os.path.join('images')
app.config['SECRET_KEY'] = 'cuhuytien3003'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
 
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


product_schema = ProductSchema()
products_schema = ProductSchema(many=True)

product = Blueprint('product', __name__)

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args.get('token')

        if not token:
            return jsonify({'message':'token is missing!'}), 403
        try:
            data = jwt.encode().decode(token, app.config['SECRET_KEY'])
        except:
            return jsonify({'message':'Token is invalid'}),403
        return f(*args , **kwargs)
    return decorated



@product.route('/get-product-by-id/<int:id>/', methods=['GET','OPTIONS'])
def get_id_product(id):
   
    
    if request.method == 'GET':
        

        product = Product.query.get(id)
        if product:
            return product_schema.jsonify(product)
        else:
            return 'not found book'


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
target = os.path.join(app.root_path,UPLOAD_FOLDER)
if not os.path.isdir(target):
    os.mkdir(target)

@product.route('/post-product/', methods=['POST','GET'])

def upload_file():
        if  request.method== 'POST':
            image = request.files['image']
            print(type("image"))
            abs_file_path = '/'.join([target , image.filename])
            abs_file_path2 = '/'.join([image.filename])
            image.save(abs_file_path)
            print('in ra ' + abs_file_path)
            productname =   request.form['productname']
            price  =  request.form['price']
            description  =  request.form['description']	
            new_product = Product( abs_file_path2,productname,price,description)
            print(type(new_product))
            print(type(new_product))
            db.session.add(new_product)
            db.session.commit()


            return 'success' 
    
@product.route('/get-product/', methods=['GET'])
def getdata():
    if request.method == 'GET':
        product = Product.query.all()
        if product:
            return  products_schema.jsonify(product)
        else:
            return 'not found book'

@product.route('/sort-product-a-z/', methods=['GET'])
def sort_az():
    if request.method == 'GET':
        product_a_z = Product.query.order_by(asc('productname')).all()
        if product:
            return  products_schema.jsonify(product_a_z)
        else:
            return 'not found book'
@product.route('/sort-product-z-a/', methods=['GET'])
def sort_za():
    if request.method == 'GET':
        product_z_a = Product.query.order_by(desc('productname')).all()
        if product:
            return  products_schema.jsonify(product_z_a)
        else:
            return 'not found book'

@product.route('/un_protect', methods=['DELETE', 'GET'])
def un_protect():
    return jsonify({'message':'ok'})
@product.route('/protect', methods=['DELETE', 'GET'])
@token_required
def protect():
    return jsonify({'message':'dont ok'})
@product.route('/delete_product/<int:id>', methods=['DELETE', 'GET'])
def delete_product(id):
    product = Product.query.get_or_404(id)
    if product:
        try:
            db.session.delete(product)
            db.session.commit()
            return 'product deleted'
        except:
            db.session.rollback()
            return 'cant delete product'
    else:
        return 'not product'
@product.route('/update_product/<int:id>', methods=[ 'GET','POST'])
def update_product(id):
    if request.method == 'POST':
        product = Product.query.get(id)
        image = request.files['image']
        img = Image.open(image)
        abs_file_path = '/'.join([target , image.filename])
        abs_file_path2 = '/'.join([image.filename])

        img.save(abs_file_path)
    
        product.image = secure_filename(abs_file_path2)
 
        product.productname =  request.form.get('productname')
        product.price =  request.form.get('price')
        product.description =  request.form.get('description')
        db.session.commit() 
        return  'Product updated'
    else:
        return 'not product'

secret_key = os.environ.get('SECRET_KEY')  

@product.route('/login', methods=['GET'])

def login():
    auth = request.authorization
    if auth and auth.password == 'password':
        token = jwt.encode({'user': auth.username,'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=15)},app.config['SECRET_KEY'])
        return jsonify({'token' :token.encode().decode('UTF-8')})
    return make_response('Could not verify', 401, {'WWW-Authenticate':'Basic realm="Login Required"'})

@product.route('/image_view/<int:id>', methods=['GET','POST','OPTIONS'])

def get_file_id(id):

    if request.method == 'GET':
       
            product = Product.query.filter_by(id = id).first()
            if product:
                full_path = os.path.join(app.root_path,UPLOAD_FOLDER)
                filename = str(product.image)
                return send_from_directory(  full_path ,filename)
            else:
                return 'not image'
            























