from .extensions import ma

class ProductSchema(ma.Schema):
    class Meta:
        fields = ('id', 'image','productname', 'price', 'description')


class UserSchema(ma.Schema):
    class Meta:
        fields = ('id', 'image','firstname', 'lastname', 'email','address','password','phone')

class CheckoutSchema(ma.Schema):
    class Meta:
        fields = ('id',"user_id","user_first_name","user_last_name","price","address","phone","order_date","product")

class StatisticalSchema(ma.Schema):
    class Meta:
        fields = ('id',"ip")


class StatisticaluservisitSchema(ma.Schema):
    class Meta:
        fields = ('id','user_id', "date_access", "week_access","month_access")
class AdminSchema(ma.Schema):
    class Meta:
        fields = ('id','image_admin', 'username','password')