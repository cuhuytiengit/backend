import os
from dotenv import load_dotenv
load_dotenv()
SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
SECRET_KEY = 'KEY'  
SQLALCHEMY_TRACK_MODIFICATIONS = False