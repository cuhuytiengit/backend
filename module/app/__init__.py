from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
from .controller.ControllerProduct import product
from .controller.ControllerUser import user
from .controller.ControllerCheckout import checkout
from .controller.ControllerStatistical import statistical
from .controller.ControllerAdmin import admin
from .extensions import db,ma
from flask_migrate import Migrate


def create_app(config_file='config.py'):
    app = Flask(__name__)
    app.config.from_pyfile(config_file)
    app.config['TIMEZONE'] = 'Asia/HoChiMinh'
    print(app.config['SECRET_KEY'])
    db.init_app(app)
    ma.init_app(app)
    
    Migrate(app,db)


    app.register_blueprint(product)
    app.register_blueprint(user)
    app.register_blueprint(checkout)
    app.register_blueprint(statistical)
    app.register_blueprint(admin)
 

    return app 