from os import name
from flask import Flask
app = Flask(__name__)

import datetime
from sqlalchemy import DateTime
from flask_migrate import Migrate
from ..extensions import db

migrate = Migrate(app, db)
class Product(db.Model):
    __tablename__ = 'product'
    __table_args__ = {'extend_existing': True} 
    id = db.Column(db.Integer, primary_key=True,autoincrement=True )
    image = db.Column(db.String(200))
    productname = db.Column(db.String(200))
    price = db.Column(db.Integer)
    description = db.Column(db.String(500))


    def __init__(self, image,  productname, price, description):
        self.image = image
        self.productname =  productname
        self.price = price
        self.description  = description 




class User(db.Model):
    __tablename__ = 'customers'
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String(200))
    firstname = db.Column(db.String(200))
    lastname = db.Column(db.String(200))
    password = db.Column(db.String(200))
    email = db.Column(db.String(500))
    address =  db.Column(db.String(500))
    phone =  db.Column(db.String(10))
    checkouts =  db.relationship('Checkout', backref='customers')



    def __init__(self, image, firstname,lastname,email,address,phone,password):
        self.image = image
        self.firstname =  firstname
        self.lastname = lastname
        self.email  = email 
        self.address  = address 
        self.phone  = phone 
        self.password  = password 

class Checkout(db.Model):
    __tablename__ = 'checkout'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('customers.id'),
        nullable=False)
    user_first_name = db.Column(db.String(100))
    user_last_name = db.Column(db.String(100))
    price = db.Column(db.Integer)
    address =  db.Column(db.String(500))
    phone =  db.Column(db.String(10))
    order_date = db.Column(DateTime, default=datetime.datetime.utcnow)
    product = db.Column(db.PickleType, nullable=False)
    
    def __init__(self, user_id,user_first_name,user_last_name,price,address,phone,order_date,product):
        self.user_id = user_id
        self.user_first_name= user_first_name
        self.user_last_name = user_last_name
        self.price  = price 
        self.address = address
        self.phone =  phone
        self.order_date = order_date
        self.product = product




class Statistical_Visit(db.Model):
    __tablename__ = 'statistical_visit'
    id = db.Column(db.Integer, primary_key=True)
    ip =  db.Column(db.String(50))
    
    def __init__(self, ip):
        self.ip = ip
    
class Statistical_User_Visit(db.Model):
    __tablename__ = 'statistical_user_visit'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('customers.id'),
        nullable=False)
    date_access = db.Column(db.String(20))
    week_access = db.Column(db.Integer)
    month_access = db.Column(db.String(20))
    def __init__(self, user_id,date_access, week_access,month_access):
        self.user_id = user_id  
        self.date_access = date_access
        self.week_access = week_access
        self.month_access =month_access
class Admin(db.Model):
    __tablename__ = 'admin'
    id = db.Column(db.Integer, primary_key=True)
    image_admin = db.Column(db.String(200))
    username = db.Column(db.String(50))
    password = db.Column(db.String(50))


    
    








